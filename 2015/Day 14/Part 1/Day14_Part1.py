import re

InputFile = open('input.txt','r')
Content = InputFile.readlines()
InputFile.close()

TotalFlightTime = 2503

Reindeer = {}
Pattern = re.compile('(.*) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.')
for Line in Content:
	Name, Speed, TimeAtSpeed, TimeAtRest = re.match(Pattern,Line).groups()
	Reindeer[Name] = {'TimeAtSpeed':int(TimeAtSpeed),'TimeAtRest':int(TimeAtRest), 'Speed':int(Speed)}

for Deer, Characteristics in Reindeer.iteritems():
	Speed = Characteristics['Speed']
	TimeAtRest = Characteristics['TimeAtRest']
	TimeAtSpeed = Characteristics['TimeAtSpeed']
	NumberofWholeRuns = TotalFlightTime / (Characteristics['TimeAtSpeed'] + Characteristics['TimeAtRest'])
	TimeOfWholeRuns = NumberofWholeRuns * (TimeAtSpeed + TimeAtRest)
	TimeLeft = TotalFlightTime - TimeOfWholeRuns
	Distance = (NumberofWholeRuns * Speed * TimeAtSpeed) + (min(TimeLeft,TimeAtSpeed) * Speed)
	print Deer + ' went ' + str(Distance) + ' km.'
