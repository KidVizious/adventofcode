from collections import Counter
import numpy as np

def main():
    """
    Entry point
    """
    infile = open('input.txt', 'r')
    lines = []
    for index, line in enumerate(infile):
        lines.append([])
        for char in line.strip():
            lines[index].append(char)
    infile.close()
    lines = np.array(lines)
    lines = np.transpose(lines)
    for line in lines:
        print(Counter(line).most_common(1))


if __name__ == '__main__':
    main()
