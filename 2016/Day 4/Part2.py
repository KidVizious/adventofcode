import re
from collections import Counter
from string import ascii_lowercase

def main():
    """
    Program entry point
    """
    alphabet = []
    for letter in ascii_lowercase:
        alphabet.append(letter)
    pattern = re.compile(r'([a-z]+)([0-9]{3})\[([a-z]{5})\]')
    real_rooms = []
    #infile = ['aaaaa-bbb-z-y-x-123[abxyz]','a-b-c-d-e-f-g-h-987[abcde]','not-a-real-room-404[oarel]','totally-real-room-200[decoy]']
    infile = open('input.txt', 'r')
    input_data = []
    for line in infile:
        #Remove hyphen from string
        line = re.sub('-', '', line)
        #Match properly formatted line and capture each part of input
        input_data.append(re.match(pattern, line))
        
    infile.close()

    sector_id = 0
    for data in input_data:
        #Get list containing count of characters in room
        character_counts = Counter(data.group(1))
        #Calculate checksum
        checksum = ''
        while len(checksum) < 5:
            #Get most frequent key
            most_frequent = max(character_counts, key=character_counts.get)
            chars = []
            #Make a list of all keys with that count
            for key, value in character_counts.items():
                if value == character_counts[most_frequent]:
                    chars.append(key)
            chars.sort()
            del character_counts[most_frequent]
            for char in chars:
                del character_counts[char]
                if len(checksum) < 5:
                    checksum = checksum + char
        if checksum == data.group(3):
            real_rooms.append((data.group(1), data.group(2)))
            sector_id = sector_id + int(data.group(2))
    
    room_names = []
    for room in real_rooms:
        name = ''
        #Find offset after we "wrap around"
        offset = int(room[1]) % 26
        for char in room[0]:
            #Get absolute position in alphabet
            char = ord(char) - ord('a')
            if char + offset > 25:
                new_offset = (offset - (25 - char)) - 1
            else:
                new_offset = offset + 1
            name = name + alphabet[new_offset]
        print(name, room[1])
if __name__ == '__main__':
    main()
