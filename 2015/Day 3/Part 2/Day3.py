from collections import Counter

#Class to store current location as Santa moves around
class JollyOldElf:
	def __init__(self):
		self.x = 0
		self.y = 0
		self.visited = {'0,0':1}
	def MoveNorth(self):
		self.y = self.y + 1
	def MoveSouth(self):
		self.y = self.y - 1
	def MoveEast(self):
		self.x = self.x + 1
	def MoveWest(self):
		self.x = self.x - 1
	def GetString(self):
		return str(self.x) + ',' + str(self.y)
	def GetVisited(self):
		return self.visited

def SaveVisit(elf):
	#Get string representation of our current location
	CurrentHouse = elf.GetString()
	if CurrentHouse in elf.visited:
		elf.visited[CurrentHouse] = elf.visited[CurrentHouse] + 1
	else:
		elf.visited[CurrentHouse] = 1
	return

def main():
	#Create Santas
	Santa = JollyOldElf()
	RoboSanta = JollyOldElf()

	#Open file containing moves
	InputFile = open('Day3Input.txt', 'r')

	#Read all moves from file
	Moves = InputFile.read()

	#Process each move
	for Index, Move in enumerate(Moves):
		#Santa moves
		if Index % 2 == 0:
			if Move == '^':
				Santa.MoveNorth()
			elif Move == '<':
				Santa.MoveWest()
			elif Move == '>':
				Santa.MoveEast()
			else:
				Santa.MoveSouth()
			SaveVisit(Santa)
		#RoboSanta moves
		else:
			if Move == '^':
				RoboSanta.MoveNorth()
			elif Move == '<':
				RoboSanta.MoveWest()
			elif Move == '>':
				RoboSanta.MoveEast()
			else:
				RoboSanta.MoveSouth()
			SaveVisit(RoboSanta)

	#Count up all visits
	SantaCounts = Counter(Santa.GetVisited())
	RoboSantaCounts = Counter(RoboSanta.GetVisited())

	Total = dict(SantaCounts.items() + RoboSantaCounts.items())
	TotalList = list(Total)
	print len(TotalList)





if __name__ == '__main__':
	main()
