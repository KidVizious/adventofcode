KEYPAD = {
    (2, 0):1,
    (1, 1):2, (2, 1):3, (3, 1):4,
    (0, 2):5, (1, 2): 6, (2, 2): 7, (3, 2):8, (4, 2):9,
    (1, 3): 'A', (2, 3):'B', (3, 3): 'C',
    (2, 4):'D'
}


def main():
    """
    Entry point
    """
    pressed_buttons = []
    infile = open('input.txt', 'r')
    location_x = 1
    location_y = 1
    for line in infile:
        line.strip()
        for char in line:
            if char == 'U':
                if (location_x, location_y - 1) in KEYPAD:
                    location_y = location_y - 1
            elif char == 'D':
                if (location_x, location_y + 1) in KEYPAD:
                    location_y = location_y + 1
            elif char == 'L':
                if (location_x - 1, location_y) in KEYPAD:
                    location_x = location_x - 1
            elif char == 'R':
                if(location_x + 1, location_y) in KEYPAD:
                    location_x = location_x + 1
        pressed_buttons.append(KEYPAD[(location_x, location_y)])
    infile.close()
    print(pressed_buttons)

if __name__ == '__main__':
    main()
