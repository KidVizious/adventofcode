import re

def ParseLine(Line):
	matches = re.split('[xX]',Line)

	a = float(matches[0])
	b = float(matches[1])
	c = float(matches[2])

	return a,b,c

def GetAreas(l,w,h):
	a = l * w
	b = l * h
	c = w * h
	return a,b,c

def main():
	InputFile = open('Day2Input.txt','r')

	Content = InputFile.readlines()
	TotalSquareFeet = 0

	for Line in Content:
		Length, Width, Height = ParseLine(Line)
		Area1, Area2, Area3 = GetAreas(Length,Width,Height)
		Extra = min(Area1, Area2, Area3)
		TotalSquareFeet = TotalSquareFeet + (2 * Area1 + 2 * Area2 + 2 * Area3 + Extra)

	print TotalSquareFeet


if __name__ == '__main__':
	main()
