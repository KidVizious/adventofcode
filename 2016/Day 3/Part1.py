
infile = open('input.txt','r')

possible = 0
for line in infile:
    a, b, c = line.strip().split()
    a = int(a)
    b = int(b)
    c = int(c)

    if a + b > c and b + c > a and a + c > b:
        possible = possible + 1

print(possible)