KEYPAD = {
    (0, 0):1, (1, 0):2, (2, 0):3,
    (0, 1):4, (1, 1):5, (2, 1):6,
    (0, 2):7, (1, 2):8, (2, 2):9
}

def main():
    """
    Entry point
    """
    pressed_buttons = []
    infile = open('input.txt', 'r')
    location_x = 1
    location_y = 1
    for line in infile:
        line.strip()
        for char in line:
            if char == 'U':
                if (location_x, location_y - 1) in KEYPAD:
                    location_y = location_y - 1
            elif char == 'D':
                if (location_x, location_y + 1) in KEYPAD:
                    location_y = location_y + 1
            elif char == 'L':
                if (location_x - 1, location_y) in KEYPAD:
                    location_x = location_x - 1
            elif char == 'R':
                if (location_x + 1, location_y) in KEYPAD:
                    location_x = location_x + 1
        pressed_buttons.append(KEYPAD[(location_x, location_y)])
    infile.close()
    print(pressed_buttons)

if __name__ == '__main__':
    main()
