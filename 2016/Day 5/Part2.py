import hashlib
import re

def main():
    """
    Program entry point
    """
    door_id = 'reyedfim'
    pattern = re.compile(r'0{5}([0-9])([0-9a-fA-F]).*')
    password = []
    for i in range(8):
        password.append(None)
    digit = 0
    done = False
    while not done:
        string_to_hash = door_id + str(digit)
        digit = digit + 1
        door_hash = hashlib.md5()
        door_hash.update(bytearray(string_to_hash, 'UTF-8'))
        resulting_hash = str(door_hash.hexdigest())
        password_digit = re.match(pattern, str(resulting_hash))
        if password_digit != None:
            position = int(password_digit.group(1))
            if position < 8 and password[position] is None:
                password[position] = password_digit.group(2)
                print(password)
        if None not in password:
            done = True
    final = ''.join(password)
    print(final)

if __name__ == '__main__':
    main()