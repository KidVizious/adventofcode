import re

def ParseLine(Line):
	matches = re.split('[xX]',Line)

	a = float(matches[0])
	b = float(matches[1])
	c = float(matches[2])

	return a,b,c

def GetSmallestTwoSides(l,w,h):
	a = min(l,w,h)
	if a == l:
		b = min(w,h)
	elif a == w:
		b = min(l,h)
	else:
		b = min(l,w)
	return a,b

def main():
	InputFile = open('Day2Input.txt','r')

	Content = InputFile.readlines()
	TotalLinearFeet = 0

	for Line in Content:
		#Parse dimensions from input line
		Length, Width, Height = ParseLine(Line)

		#Find the smallest two sides to calculate the smallest perimeter
		Side1, Side2 = GetSmallestTwoSides(Length, Width, Height)

		#Calculate the volume of the gift for the "extra" ribbon
		Extra = Length * Width * Height

		#Add ribbon needed for this present to the total
		TotalLinearFeet = TotalLinearFeet + (2 * Side1 + 2 * Side2 + Extra)

	print TotalLinearFeet


if __name__ == '__main__':
	main()
