import re
from collections import Counter

def main():
    """
    Program entry point
    """
    pattern = re.compile(r'([a-z]+)([0-9]{3})\[([a-z]{5})\]')
    #infile = ['aaaaa-bbb-z-y-x-123[abxyz]','a-b-c-d-e-f-g-h-987[abcde]','not-a-real-room-404[oarel]','totally-real-room-200[decoy]']
    infile = open('input.txt', 'r')
    input_data = []
    for line in infile:
        #Remove hyphen from string
        line = re.sub('-', '', line)
        #Match properly formatted line and capture each part of input
        input_data.append(re.match(pattern, line))
        
    infile.close()

    sector_id = 0
    for data in input_data:
        #Get list containing count of characters in room
        character_counts = Counter(data.group(1))
        #Calculate checksum
        checksum = ''
        while len(checksum) < 5:
            #Get most frequent key
            most_frequent = max(character_counts, key=character_counts.get)
            chars = []
            #Make a list of all keys with that count
            for key, value in character_counts.items():
                if value == character_counts[most_frequent]:
                    chars.append(key)
            chars.sort()
            del character_counts[most_frequent]
            print(chars)
            print(character_counts)
            for char in chars:
                del character_counts[char]
                if len(checksum) < 5:
                    checksum = checksum + char
        print(checksum, data.group(3))
        if checksum == data.group(3):
            sector_id = sector_id + int(data.group(2))
    print(sector_id)
      
      
        
if __name__ == '__main__':
    main()
