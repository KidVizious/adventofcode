import re
import json
from time import sleep

def my_decoder(obj):

	for value in obj.itervalues():
		if value == 'red':
			return {}
	return obj

InputFile = open('input.sdx','r')
content = InputFile.read().strip()
InputFile.close()

decoder = json.JSONDecoder(object_hook = my_decoder)
decoded_json = decoder.decode(content)

content = str(decoded_json)

numbers = re.findall('-?\d+',content)
ans = 0
for number in numbers:
	ans += int(number)
print ans
