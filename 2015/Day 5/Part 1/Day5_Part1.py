from collections import Counter
import re

def main():
	InputFile = open('Day5Input.txt','r')

	NoNaughtyStrings = []

	pattern = re.compile(r'ab|cd|pq|xy')
	for line in InputFile:
			if pattern.search(line.lower()) == None:
				NoNaughtyStrings.append(line)

	print len(NoNaughtyStrings)

	AtLeastThreeVowels = []

	for line in NoNaughtyStrings:
		ll = list(line.lower())
		lc = Counter(ll)
		NumVowels = lc['a'] + lc['e'] + lc['i'] + lc['o'] + lc['u']
		if NumVowels >= 3:
			AtLeastThreeVowels.append(line)

	print len(AtLeastThreeVowels)

	RepeatedCharacter = []

	pattern = re.compile(r'(.)\1{1,}')

	for line in AtLeastThreeVowels:
		if pattern.search(line.lower()):
			RepeatedCharacter.append(line)

	print len(RepeatedCharacter)

	return

if __name__ == '__main__':
	main()
