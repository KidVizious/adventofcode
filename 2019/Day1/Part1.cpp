#include <fstream>
#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " filename." << std::endl;
        return -1;
    }

    std::ifstream file;
    file.open(argv[1], std::ifstream::in);

    uint32_t totalFuel{};

    for (std::string line; std::getline(file, line); )
    {
        uint32_t fuel = stoi(line, nullptr, 10);
        fuel = std::floor(fuel / 3) - 2;
        totalFuel += fuel;
    }
    file.close();
    std::cout << "Fuel required: " << totalFuel << std::endl;
}