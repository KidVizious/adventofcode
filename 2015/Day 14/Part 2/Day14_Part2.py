import re
from time import sleep

InputFile = open('input.txt','r')
Content = InputFile.readlines()
InputFile.close()

#Time for race
TotalFlightTime = 2503

#Dictionary to hold info on each Reindeer
Reindeer = {}

#Pattern of lines in input file
Pattern = re.compile('(.*) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.')

#Fill up data structure
for Line in Content:
	Name, Speed, TimeAtSpeed, TimeAtRest = re.match(Pattern,Line).groups()
	Reindeer[Name] = {'TimeAtSpeed':int(TimeAtSpeed),'TimeAtRest':int(TimeAtRest), 'Speed':int(Speed),'Flying':True,'Distance':int(0),'Score':int(0),'StartTime':int(0),'Duration':int(TimeAtSpeed)}

#Run test for TotalFlightTime seconds
for Time in range(1,TotalFlightTime + 1):
	#Calculate distance thus far for each deer
	for Deer, State in Reindeer.iteritems():
		if State['Flying'] == True:
			if abs(Time - State['StartTime']) >= State['Duration']:
				State['StartTime'] = Time
				State['Duration'] = State['TimeAtRest']
				State['Flying'] = False
			State['Distance'] += State['Speed']
		elif abs(Time - State['StartTime']) >= State['Duration']:
			State['StartTime'] = Time
			State['Duration'] = State['TimeAtSpeed']
			State['Flying'] = True
	Furthest = max(Reindeer,key=lambda x: Reindeer[x]['Distance'])
	for Deer, State in Reindeer.iteritems():
		if State['Distance'] >= Reindeer[Furthest]['Distance']:
			State['Score'] += 1
for k,v in Reindeer.iteritems():
	print v['Score']
