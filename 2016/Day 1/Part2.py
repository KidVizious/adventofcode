visited = set()

def turn(current_direction, instruction):
    """
    Turn according to instructions
    """
    if current_direction == 'N':
        if 'R' in instruction:
            newdir = 'E'
        else:
            newdir = 'W'
    elif current_direction == 'S':
        if 'R' in instruction:
            newdir = 'W'
        else:
            newdir = 'E'
    elif current_direction == 'E':
        if 'R' in instruction:
            newdir = 'S'
        else:
            newdir = 'N'
    elif current_direction == 'W':
        if 'R' in instruction:
            newdir = 'N'
        else:
            newdir = 'S'

    return newdir

def takestep(current_direction, instruction, current_x, current_y):
    """
    Take a step in the right direction
    """
    distance = instruction[1:]

    distance = int(distance)

    for i in range(distance):
        if current_direction == 'N':
            current_y = current_y + 1
        elif current_direction == 'S':
            current_y = current_y - 1
        elif current_direction == 'E':
            current_x = current_x + 1
        else:
            current_x = current_x -1
        if (current_x, current_y) in visited:
            print('Duplicate position at: ' + str(abs(current_x) + abs(current_y)))
        else:
            visited.add((current_x, current_y))

    return current_x, current_y

def main():
    """
    Entry Point
    """

    currentdir = 'N'
    current_x = 0
    current_y = 0

    infile = open('input.txt', 'r')
    directions = infile.read().strip().split(',')
    infile.close()

    for direction in directions:
        direction = direction.strip()
        currentdir = turn(currentdir, direction)
        current_x, current_y = takestep(currentdir, direction, current_x, current_y)

    print(abs(current_x) + abs(current_y))

    return

if __name__ == '__main__':
    main()
