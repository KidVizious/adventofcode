from collections import Counter
import numpy as np

def main():
    """
    Entry point
    """
    infile = open('input.txt', 'r')
    lines = []
    for index, line in enumerate(infile):
        lines.append([])
        for char in line.strip():
            lines[index].append(char)
    infile.close()
    lines = np.array(lines)
    lines = np.transpose(lines)
    for line in lines:
        count = Counter(line)
        print(min(count, key = count.get))


if __name__ == '__main__':
    main()
