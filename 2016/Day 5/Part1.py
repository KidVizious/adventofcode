import hashlib
import re

def main():
    """
    Program entry point
    """
    door_id = 'reyedfim'
    pattern = re.compile(r'0{5}([0-9a-fA-F]).*')
    password = ''
    digit = 0
    for i in range(8):
        done = False
        print('Looking for character: ', i)
        while not done:
            string_to_hash = door_id + str(digit)
            digit = digit + 1
            door_hash = hashlib.md5()
            door_hash.update(bytearray(string_to_hash, 'UTF-8'))
            resulting_hash = str(door_hash.hexdigest())
            password_digit = re.match(pattern, str(resulting_hash))
            if password_digit != None:
                print(resulting_hash)
                password = password + password_digit.group(1)
                print(password)
                done = True
    print(password)

if __name__ == '__main__':
    main()
