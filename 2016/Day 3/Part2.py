infile = open('input.txt', 'r')

possible = 0

a = []
b = []
c = []
for line in infile:
    x, y, z = line.strip().split()
    a.append(int(x))
    b.append(int(y))
    c.append(int(z))
infile.close()

triangles = a + b + c

for i in range(0, len(triangles), 3):
    if triangles[i] + triangles[i + 1] > triangles[i + 2] \
    and triangles[i] + triangles[i + 2] > triangles[i + 1] \
    and triangles[i + 1] + triangles[i + 2] > triangles[i]:
        possible = possible + 1

print(possible)
