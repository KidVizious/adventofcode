#include <fstream>
#include <iostream>
#include <cmath>

int CalculateFuelForModule(int fuel)
{
    int newFuel{static_cast<int>(std::floor(fuel / 3)) - 2};
    
    if (newFuel > 0)
    {
        return newFuel + CalculateFuelForModule(newFuel);
    }
    return 0;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " filename." << std::endl;
        return -1;
    }

    std::ifstream file;
    file.open(argv[1], std::ifstream::in);

    unsigned int totalFuel{};

    for (std::string line; std::getline(file, line); )
    {
        int fuel = stoi(line, nullptr, 10);

        totalFuel += CalculateFuelForModule(fuel);
        
    }

    file.close();
    std::cout << "Fuel required: " << totalFuel << std::endl;
}