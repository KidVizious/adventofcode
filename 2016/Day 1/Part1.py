
def turn(current_direction, instruction):

    if current_direction == 'N':
        if 'R' in instruction:
            newdir = 'E'
        else:
            newdir = 'W'
    elif current_direction == 'S':
        if 'R' in instruction:
            newdir = 'W'
        else:
            newdir = 'E'
    elif current_direction == 'E':
        if 'R' in instruction:
            newdir = 'S'
        else:
            newdir = 'N'
    elif current_direction == 'W':
        if 'R' in instruction:
            newdir = 'N'
        else:
            newdir = 'S'

    return newdir

def takestep(current_direction, instruction, currentX, currentY):

    distance = instruction[1:]

    distance = int(distance)
    print(distance)

    if current_direction == 'N':
        currentY = currentY + distance
    elif current_direction == 'S':
        currentY = currentY - distance
    elif current_direction == 'E':
        currentX = currentX + distance
    else:
        currentX = currentX - distance

    return currentX, currentY

def main():
    """
    Entry Point
    """

    currentdir = 'N'
    currentX = 0
    currentY = 0

    infile = open('input.txt', 'r')

    directions = infile.read().strip().split(',')
    infile.close()
    for direction in directions:
        direction = direction.strip()
        currentdir = turn(currentdir, direction)
        currentX, currentY = takestep(currentdir, direction, currentX, currentY)

    print(currentX, currentY)
    print(abs(currentX) + abs(currentY))

    return

if __name__ == '__main__':
    main()
