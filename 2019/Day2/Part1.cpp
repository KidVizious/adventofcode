#include <fstream>
#include <iostream>
#include <vector>


static constexpr int INSTRUCTION_LENGTH{4};

enum class Ops : int
{
    Add = 1,
    Multiply = 2,
    Halt = 99
};

void RunProgram(std::vector<int> &program)
{
    int pc{};
    int op{program[pc]};

    while(op != static_cast<int>(Ops::Halt))
    {
        auto first = program[pc + 1];
        auto second = program[pc + 2];
        auto location = program[pc + 3];
        if (op == static_cast<int>(Ops::Add))
        {
            program[location] = program[first] + program[second];
        }
        else if (op == static_cast<int>(Ops::Multiply))
        {
            program[location] = program[first] * program[second];
            
        }
        pc += INSTRUCTION_LENGTH;
        op = program[pc];
    }
}


int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " <filename>" << std::endl;
        return -1;
    }

    auto &file = *new std::ifstream(argv[1]);

    if (!file.is_open())
    {
        delete &file;
        return -2;
    }


    auto p{new std::vector<int>};
    while(file)
    {
        int i;
        char junk; // Throw away commas
        file >> i >> junk;
        p->push_back(i);
    }

    file.close();
    
    RunProgram(*p);

    std::cout << "Resut: " << p->front() << "\n";

    return 0;
}